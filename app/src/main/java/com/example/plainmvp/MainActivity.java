package com.example.plainmvp;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.plainmvp.adapter.UserListAdapter;
import com.example.plainmvp.login.LoginContract;
import com.example.plainmvp.login.LoginPresenter;
import com.example.plainmvp.login.User;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements LoginContract.View {

    @BindView(R.id.edt_username)
    EditText edtUsername;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.btn_add)
    Button btnAdd;

    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        loginPresenter = new LoginPresenter(this);
    }

    @OnClick({R.id.edt_username, R.id.edt_password, R.id.btn_submit, R.id.btn_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edt_username:
                break;
            case R.id.edt_password:
                break;
            case R.id.btn_submit:
                loginPresenter.validateLogin(edtUsername.getText().toString(), edtPassword.getText().toString());
                break;
            case R.id.btn_add:
                loginPresenter.addUser(edtUsername.getText().toString(), edtPassword.getText().toString());
                break;
        }
    }

    @Override
    public void showSuccess(String message) {
        this.showLoading(false);
        Toast.makeText(this, "Success!" + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String error) {
        this.showLoading(false);
        Toast.makeText(this, "Fail : " + error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(boolean loading) {

        if (loading) {
            if (progress.getVisibility() != View.VISIBLE) {
                progress.setVisibility(View.VISIBLE);
            }
        } else {
            if (progress.getVisibility() != View.GONE) {
                progress.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void showList(ArrayList<User> userList) {


        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_user_list);
        dialog.setTitle("User List");
        RecyclerView list = dialog.findViewById(R.id.recyclerView);
        //TextView txtView = dialog.findViewById(R.id.txt_result);

        if (userList!=null&&userList.size()>0){
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            list.setHasFixedSize(true);
            list.setLayoutManager(mLayoutManager);
            UserListAdapter adapter = new UserListAdapter(userList);
            list.setAdapter(adapter);
        }else {
            //
        }



        dialog.show();
    }
}
