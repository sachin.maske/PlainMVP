package com.example.plainmvp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.plainmvp.R;
import com.example.plainmvp.login.User;

import java.util.List;

/**
 * Created by webwerks1 on 6/2/18.
 */

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.MyViewHolder> {

    private List<User> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView user, password;

        public MyViewHolder(View view) {
            super(view);
            user = (TextView) view.findViewById(R.id.txt_name);
            password = (TextView) view.findViewById(R.id.txt_password);
        }
    }


    public UserListAdapter(List<User> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        User user = moviesList.get(position);
        holder.user.setText(user.getUserName());
        holder.password.setText(user.getPassword());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
