package com.example.plainmvp.utill;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import com.example.plainmvp.login.LoginContract;
import com.example.plainmvp.login.User;
import com.google.gson.Gson;

/**
 * Created by webwerks1 on 6/2/18.
 */

public class PreferenceUtill {

    public static void saveUser(String users, Context context) {

        Log.d("DRJ", "usrs save :" + users);


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("users", users);
        editor.commit();
    }

    public static String getUsers(Context view) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(view.getApplicationContext());
        String str_user = preferences.getString("users", "");
        Log.d("DRJ", "usrs get :" + str_user);
        return str_user;

    }
}
