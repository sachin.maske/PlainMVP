package com.example.plainmvp.login;


import android.content.Context;
import android.os.Handler;
import android.view.View;

import com.example.plainmvp.utill.PreferenceUtill;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by webwerks1 on 6/2/18.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private final LoginContract.View loginView;
    private Context context; //ToDO Needs to decide later

    public LoginPresenter(LoginContract.View loginView) {
        this.loginView = loginView;
        this.context =(Context) loginView;
    }

    @Override
    public void validateLogin(String username, String password) {
        loginView.showLoading(true);

        if (username != null && username.trim().length() > 0) {

            if (password != null && password.trim().length() > 0) {

                if (username.equalsIgnoreCase("admin") && password.equalsIgnoreCase("admin")) {

                    Handler handler = new Handler();
                    handler.postDelayed(runnable, 5000);

                } else {
                    loginView.showError("wrong credentials!");
                }
            } else {

                loginView.showError("invalid password!");
            }
        } else {

            loginView.showError("invalid user name!");
        }
    }

    @Override
    public void addUser(String username, String password) {
        loginView.showLoading(true);
        String users = "";

        if (username != null && username.trim().length() > 0) {

            if (password != null && password.trim().length() > 0) {


                users = PreferenceUtill.getUsers(context);

                Type type = new TypeToken<List<User>>() {
                }.getType();
                if (users.trim().length() > 0) {
                    List<User> userList = new Gson().fromJson(users, type);
                    userList.add(new User(username, password));
                    PreferenceUtill.saveUser(new Gson().toJson(userList), context);
                } else {
                    List<User> userList = new ArrayList<>();
                    userList.add(new User(username, password));
                    PreferenceUtill.saveUser(new Gson().toJson(userList), context);
                }

                loginView.showSuccess("Addition of users: " + PreferenceUtill.getUsers(context));
                //loginView.showList(PreferenceUtill.getUsers(context));
                return;


            } else {

                loginView.showError("invalid password!");
            }
        } else {

            loginView.showError("invalid user name!");
        }
    }


    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            loginView.showSuccess("Welcome admin");
            Type type = new TypeToken<List<User>>() {
            }.getType();
            ArrayList<User> userList = new Gson().fromJson(PreferenceUtill.getUsers(context), type);
            loginView.showList(userList);
            return;

        }
    };
}
