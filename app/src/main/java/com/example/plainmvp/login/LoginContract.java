package com.example.plainmvp.login;

import java.util.ArrayList;

/**
 * Created by webwerks1 on 6/2/18.
 */

public interface LoginContract {

    /**
     * Represents the View in MVP.
     */
    interface View {
        void showSuccess(String message);

        void showError(String error);

        void showLoading(boolean loading);

        void showList(ArrayList<User> userList);
    }

    /**
     * Represents the Presenter in MVP.
     */
    interface Presenter {
        void validateLogin(String username, String password);

        void addUser(String username, String password);
    }
}
